from django.contrib import admin

from .models import PlantGallery

# Register your models here.
admin.site.register(PlantGallery)