from rest_framework import serializers

from .models import *


# Code from @glemmaPaul
class StdImageField(serializers.ImageField):

    def to_native(self, obj):
        return self.get_variations_urls(obj)

    def to_representation(self, obj):
        return self.get_variations_urls(obj)

    def get_variations_urls(self, obj):

        return_object = {}

        field = obj.field

        if hasattr(field, 'variations'):
            variations = field.variations

            for key in variations.keys():
                if hasattr(obj, key):
                    field_obj = getattr(obj, key, None)
                    if field_obj and hasattr(field_obj, 'url'):
                        return_object[key] = super(StdImageField, self).to_representation(field_obj)

        if hasattr(obj, 'url'):
            return_object['original'] = super(StdImageField, self).to_representation(obj)

        return return_object


class PlantGallerySerializer(serializers.ModelSerializer):

    image = StdImageField()

    class Meta:
        model = PlantGallery
        fields = ['image', ]


class DiseaseGallerySerializer(serializers.ModelSerializer):

    image = StdImageField()

    class Meta:
        model = DiseaseGallery
        fields = ['image', ]
