from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters import rest_framework as filters

from .models import *
from .serializers import *


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 30
    page_size_query_param = 'page_size'
    max_page_size = 30

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'total_pages': self.page.paginator.num_pages,
            'results': data
        })


class PlantViewSet(viewsets.ModelViewSet):
    queryset = Plant.objects.all()
    serializer_class = PlantSerializer
    filter_backends = [filters.DjangoFilterBackend, SearchFilter, OrderingFilter]
    filter_fields = ['plantations__months_region__months__month__number', 'plantations__months_region__region__id',
                     'categories__id', 'lightness__id', 'climes__id', 'life_cycle__id']
    search_fields = ['popular_names__popular_name', 'scientific_name', 'varieties__variety_name']
    pagination_class = StandardResultsSetPagination

    lookup_field = 'slug'


class PlantationViewSet(viewsets.ModelViewSet):
    queryset = Plantation.objects.all()
    serializer_class = PlantationSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filter_fields = ['plant__slug', 'months_region__region__name']


class MonthViewSet(viewsets.ModelViewSet):
    queryset = Month.objects.all()
    serializer_class = MonthSerializer


class RegionViewSet(viewsets.ModelViewSet):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer


class LightnessViewSet(viewsets.ModelViewSet):
    queryset = Lightness.objects.all()
    serializer_class = LightnessSerializer


class LifeCycleViewSet(viewsets.ModelViewSet):
    queryset = LifeCycle.objects.all()
    serializer_class = LifeCycleSerializer


class ClimeViewSet(viewsets.ModelViewSet):
    queryset = Clime.objects.all()
    serializer_class = ClimeSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class PlantVarietyViewSet(viewsets.ModelViewSet):
    queryset = PlantVariety.objects.all()
    serializer_class = PlantVarietySerializer

    lookup_field = 'slug'


class CharacteristicsViewSet(viewsets.ModelViewSet):
    queryset = Plant.objects.all()
    serializer_class = CharacteristicSerializer

    lookup_field = 'slug'


class CharacteristicsVarietyViewSet(viewsets.ModelViewSet):
    queryset = PlantVariety.objects.all()
    serializer_class = CharacteristicVarSerializer

    lookup_field = 'slug'


class SyntrophyViewSet(viewsets.ModelViewSet):
    queryset = Syntrophy.objects.all()
    serializer_class = SyntrophySerializer

    filter_backends = [filters.DjangoFilterBackend]
    filter_fields = ['plant1__slug']


class DiseaseAffectViewSet(viewsets.ModelViewSet):
    queryset = DiseaseAffect.objects.all()
    serializer_class = DiseaseAffectSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filter_fields = ['plant__slug']


