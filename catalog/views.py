import json

from django.shortcuts import render, get_object_or_404

from .models import Plant, PopularName, LifeCycle, PlantVariety
from .serializers import PlantSerializer


# Create your views here.
def plants(request):
    return render(request, 'catalog/home.html', {})


def plant_detail(request, slug):
    #plant = get_object_or_404(Plant, slug=slug)

    plant = Plant.objects.get(slug=slug)

    popular_names = PopularName.objects.filter(plant=plant)
    context = {'plant': plant,
               'popular_names': popular_names,
               'popular_name': popular_names[0]}

    return render(request, 'catalog/plant_detail.html', context)
